#!/bin/bash

die() {
	ra=$1
	shift
	echo "$*" >&2
	exit ${ra}
}

tag=$1

sed \
	-e "s@#{ROS_TAG}@${tag}@g"\
	Dockerfile.tpl > Dockerfile.${tag}

docker build --pull \
	-f Dockerfile.${tag} -t armv7hf-ros:${tag} \
	$(dirname $0)

# TODO: Make optional
rm Dockerfile.${tag}
